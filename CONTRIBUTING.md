Ansible
=======

Requirements
------------

Follow the [installation instructions](README.md#installation).

For testing with libvirt, install `virt-install` and `edk2-ovmf`.

Test'n dev
----------

Build with Docker/Podman and create an alias :

```sh
sudo docker build -t gwerlas/preseed-embedder:latest .
alias preseed-embedder="PRESEED_EMBEDDER_REPO=localhost $PWD/container-wrapper"
```

Build your ISO image with your preseed and test it :

```sh
name=french-uefi
preseed-embedder -p tests/$name.cfg -o tests/$name.iso
```

Test the quiet installation with `virt-install` :

```sh
virt-install \
  --connect qemu:///system \
  --name $name \
  --memory 2048 \
  --vcpus 1 \
  --os-variant debian12 \
  --virt-type kvm \
  --network network=default,model=virtio \
  --graphics vnc \
  --disk size=10,bus=virtio,format=qcow2 \
  --cdrom tests/$name.iso \
  --boot uefi
```

Cleanup :

```sh
virsh \
  --connect qemu:///system \
  destroy $name
virsh \
  --connect qemu:///system \
  undefine $name \
  --remove-all-storage \
  --nvram
rm -f tests/$name.iso
```
