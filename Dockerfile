FROM debian:12

# Dependencies
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        cpio genisoimage syslinux-utils isolinux xorriso \
        ca-certificates debian-keyring gnupg p7zip-full wget && \
    rm -rf /var/lib/apt/lists/*

# https://www.debian.org/CD/verify
RUN for i in F41D30342F3546695F65C66942468F4009EA8AC3 DF9B9C49EAA9298432589D76DA87E80D6294BE9B 10460DAD76165AD81FBC0CE9988021A964E6EA7D; do \
        gpg --keyserver keyring.debian.org --recv-keys 0x$i; \
    done

# Preseed embedder installation
ADD              files/etc/*   /etc/
ADD              files/share/* /usr/share/preseed-embedder/
ADD --chmod=755  files/bin/*   /usr/local/bin/
