Preseed embedder
================

[![pipeline status](https://gitlab.com/yoanncolin/scripts/preseed-embedder/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/scripts/preseed-embedder/-/commits/main)

Embed preseed in Debian ISO image.

From the [Debian 11 image][].

GitLab Project : [yoanncolin/container-images/preseed-embedder][].

[Debian 11 image]: https://hub.docker.com/_/debian
[yoanncolin/container-images/preseed-embedder]: https://gitlab.com/yoanncolin/container-images/preseed-embedder

Requirements
------------

Install Docker or Podman.

Because the ISO contains filesystem nodes, the container must run as `root`. In rootless mode, be sure to configure appropriatly Sudo.

Installation
------------

### Rootfull or almost (sudo all)

```sh
curl \
  -o /usr/local/bin/preseed-embedder \
  https://gitlab.com/yoanncolin/scripts/preseed-embedder/-/raw/main/container-wrapper \
chmod +x /usr/local/bin/preseed-embedder
```

### Restrictive sudo

In case You are in rootless mode and your Sudo is configured to be restrictive,
You can deploy the wrapper like this :

1. As root, download the wrapper in a restricted area :
   
   ```sh
   mkdir /root/bin
   curl \
     -o /root/bin/preseed-embedder \
     https://gitlab.com/yoanncolin/scripts/preseed-embedder/-/raw/main/container-wrapper \
   chmod +x /root/bin/preseed-embedder
   ```

2. Create an intermediate wrapper for your users :

   ```sh
   echo 'sudo /root/bin/preseed-embedder "$@"' > /usr/local/bin/preseed-embedder
   chmod +x /usr/local/bin/preseed-embedder
   ```

3. Create the sudo rule :

   ```sh
   echo 'Defaults    env_keep += "PRESEED_EMBEDDER_REPO PRESEED_EMBEDDER_VERSION"' > /etc/sudoers.d/preseed-embedder
   echo '%wheel      ALL=(ALL)   NOPASSWD: /root/bin/preseed-embedder *' >> /etc/sudoers.d/preseed-embedder
   chmod 600 /etc/sudoers.d/preseed-embedder
   ```

Usage
-----

Display the help message :

```sh
preseed-embedder -h
```

Embedding your preseed in a new Debian ISO :

```sh
preseed-embedder -p "<your-preseed-path>" -o "<expected-iso-path>"
```

If no argument is given, `preseed-embedder` will embed the
[Stable example preseed][] in the latest stable Debian netinst ISO.

[Stable example preseed]: https://www.debian.org/releases/stable/example-preseed.txt

License
-------

[BSD 3-Clause License](LICENSE).

Credits
-------

Inspired from [Preseed creator](https://framagit.org/fiat-tux/hat-softwares/preseed-creator).
